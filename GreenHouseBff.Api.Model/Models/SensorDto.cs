﻿using System;

namespace GreenHouseBff.Api.Model.Models
{
    public class SensorDto
    {
        public Guid SensorId { get; set; }

        public string SensorName { get; set; }

        public string Description { get; set; }

        public DateTimeOffset CreationDate { get; set; }

        public double Value { get; set; }
    }
}