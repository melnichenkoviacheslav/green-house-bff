﻿using AutoMapper;
using GreenHouseBff.Api.Model.Models;
using GreenHouseBff.Data.Model;

namespace GreenHouseBff.Api.Mapping.Profiles
{
    public class SensorsProfile : Profile
    {
        public SensorsProfile()
        {
            CreateMap<Sensor, SensorDto>();
        }
    }
}