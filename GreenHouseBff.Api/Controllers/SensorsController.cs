﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using GreenHouseBff.Api.Model.Models;
using GreenHouseBff.Domain.Sensors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GreenHouseBff.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SensorsController : ControllerBase
    {
        private readonly ILogger<SensorsController> _logger;
        private readonly ISensorsOrchestrator _sensorsOrchestrator;
        private readonly IMapper _mapper;

        public SensorsController(
            ISensorsOrchestrator sensorsOrchestrator,
            IMapper mapper,
            ILogger<SensorsController> logger)
        {
            _logger = logger;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _sensorsOrchestrator = sensorsOrchestrator ?? throw new ArgumentNullException(nameof(sensorsOrchestrator));
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<SensorDto>), 200)]
        public async Task<ActionResult<IEnumerable<SensorDto>>> GetSensors()
        {
            var sensorsFromRepo = await _sensorsOrchestrator.GetSensorsAsync();
            return Ok(_mapper.Map<IEnumerable<SensorDto>>(sensorsFromRepo));
        }
    }
}