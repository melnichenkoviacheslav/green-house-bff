﻿using Autofac;
using GreenHouseBff.Domain.Sensors;
using GreenHouseBff.Repository.DataContext;

namespace GreenHouseBff.Api.Autofac
{
    public class ApiDependencyModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            RegisterTypes(builder);
        }

        private static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<GreenHouseBffRepository>().As<IGreenHouseBffRepository>().InstancePerLifetimeScope();
            builder.RegisterType<SensorsOrchestrator>().As<ISensorsOrchestrator>().InstancePerLifetimeScope();
        }
    }
}