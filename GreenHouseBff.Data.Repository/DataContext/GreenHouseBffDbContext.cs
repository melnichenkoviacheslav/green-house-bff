﻿using System;
using System.Collections.Generic;
using GreenHouseBff.Data.Model;
using Microsoft.EntityFrameworkCore;

namespace GreenHouseBff.Repository.DataContext
{
    public class GreenHouseBffDbContext : DbContext
    {
        public GreenHouseBffDbContext(DbContextOptions<GreenHouseBffDbContext> options) : base(options)
        {
        }

        public DbSet<Sensor> Sensors { get; set; }
        //public DbSet<Measurement> Measurements { get; set; }

        /// <summary>
        /// For testing purpose only!
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");

            modelBuilder.Entity<Sensor>().HasData(
                 new Sensor
                 {
                     SensorId = Guid.Parse("1902b665-1190-4c70-9915-b9c2d7680450"),
                     SensorName = "Temperature1",
                     Description = "Sensor in top of green house",
                     CreationDate = DateTimeOffset.Now,
                     Value = 34.4
                 },
                 new Sensor
                 {
                     SensorId = Guid.Parse("2902b665-1190-4c70-9915-b9c2d7680450"),
                     SensorName = "Temperature2",
                     Description = "Sensor in bottom of green house",
                     CreationDate = DateTimeOffset.Now,
                     Value = 54.4
                 },
                 new Sensor
                 {
                     SensorId = Guid.Parse("3902b665-1190-4c70-9915-b9c2d7680450"),
                     SensorName = "Humidity",
                     Description = "Humidity sensor",
                     CreationDate = DateTimeOffset.Now,
                     Value = 14.4
                 },
                 new Sensor
                 {
                     SensorId = Guid.Parse("4902b665-1190-4c70-9915-b9c2d7680450"),
                     SensorName = "Illuminance",
                     Description = "Illuminance sensor",
                     CreationDate = DateTimeOffset.Now,
                     Value = 84.4
                 }
             );

            base.OnModelCreating(modelBuilder);
        }
    }
}