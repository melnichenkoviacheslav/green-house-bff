﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GreenHouseBff.Data.Model;
using Microsoft.EntityFrameworkCore;

namespace GreenHouseBff.Repository.DataContext
{
    public class GreenHouseBffRepository : IGreenHouseBffRepository, IDisposable
    {
        private readonly GreenHouseBffDbContext _dbContext;

        public GreenHouseBffRepository(GreenHouseBffDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public Sensor GetSensor(Guid sensorId)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Sensor>> GetSensorsDataAsync()
        {
            return await _dbContext.Sensors.ToListAsync();
        }

        public bool Save()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
        }
    }
}