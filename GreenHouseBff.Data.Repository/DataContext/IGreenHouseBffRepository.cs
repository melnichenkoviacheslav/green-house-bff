﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GreenHouseBff.Data.Model;

namespace GreenHouseBff.Repository.DataContext
{
    public interface IGreenHouseBffRepository
    {
        Sensor GetSensor(Guid sensorId);
        Task<IEnumerable<Sensor>> GetSensorsDataAsync();
    }
}