﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GreenHouseBff.Repository.Migrations
{
    public partial class Base : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateTable(
                name: "Sensors",
                schema: "public",
                columns: table => new
                {
                    SensorId = table.Column<Guid>(type: "uuid", nullable: false),
                    SensorName = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false),
                    CreationDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    Value = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sensors", x => x.SensorId);
                });

            migrationBuilder.InsertData(
                schema: "public",
                table: "Sensors",
                columns: new[] { "SensorId", "CreationDate", "Description", "SensorName", "Value" },
                values: new object[,]
                {
                    { new Guid("1902b665-1190-4c70-9915-b9c2d7680450"), new DateTimeOffset(new DateTime(2021, 5, 19, 23, 46, 15, 79, DateTimeKind.Unspecified).AddTicks(6007), new TimeSpan(0, 3, 0, 0, 0)), "Sensor in top of green house", "Temperature1", 34.399999999999999 },
                    { new Guid("2902b665-1190-4c70-9915-b9c2d7680450"), new DateTimeOffset(new DateTime(2021, 5, 19, 23, 46, 15, 80, DateTimeKind.Unspecified).AddTicks(9886), new TimeSpan(0, 3, 0, 0, 0)), "Sensor in bottom of green house", "Temperature2", 54.399999999999999 },
                    { new Guid("3902b665-1190-4c70-9915-b9c2d7680450"), new DateTimeOffset(new DateTime(2021, 5, 19, 23, 46, 15, 80, DateTimeKind.Unspecified).AddTicks(9916), new TimeSpan(0, 3, 0, 0, 0)), "Humidity sensor", "Humidity", 14.4 },
                    { new Guid("4902b665-1190-4c70-9915-b9c2d7680450"), new DateTimeOffset(new DateTime(2021, 5, 19, 23, 46, 15, 80, DateTimeKind.Unspecified).AddTicks(9923), new TimeSpan(0, 3, 0, 0, 0)), "Illuminance sensor", "Illuminance", 84.400000000000006 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Sensors",
                schema: "public");
        }
    }
}
