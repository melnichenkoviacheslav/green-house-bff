﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GreenHouseBff.Data.Model;

namespace GreenHouseBff.Domain.Sensors
{
    public interface ISensorsOrchestrator
    {
        Task<IEnumerable<Sensor>> GetSensorsAsync();
    }
}