﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using GreenHouseBff.Data.Model;
using GreenHouseBff.Repository.DataContext;

namespace GreenHouseBff.Domain.Sensors
{
    public class SensorsOrchestrator : ISensorsOrchestrator
    {
        private readonly IGreenHouseBffRepository _greenHouseBffRepository;

        public SensorsOrchestrator(IGreenHouseBffRepository greenHouseBffRepository)
        {
            _greenHouseBffRepository = greenHouseBffRepository;
        }

        public Task<IEnumerable<Sensor>> GetSensorsAsync()
        {
            return _greenHouseBffRepository.GetSensorsDataAsync();
        }
    }
}