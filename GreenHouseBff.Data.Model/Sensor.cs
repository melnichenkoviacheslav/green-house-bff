﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GreenHouseBff.Data.Model
{
    public class Sensor
    {
        [Key]
        public Guid SensorId { get; set; }

        [Required]
        [MaxLength(50)]
        public string SensorName { get; set; }

        [Required]
        public string Description { get; set; }

        //[Required]
        //public List<Measurement> MeasurementIds { get; set; }

        [Required]
        public DateTimeOffset CreationDate { get; set; }

        [Required]
        public double Value { get; set; }
    }
}